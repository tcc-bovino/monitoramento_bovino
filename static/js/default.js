var mapa;
var passo;

$(document).ready(function(){

    passo = $('#passo').val();

    for(i=0;i<=5; i++){

        if(i<passo){
            $('#p'+i).addClass("completed");
        }
        if(i == passo){
            $('#p'+i).addClass("active");
        }
        if(i>passo){
            $('#p'+i).addClass("disabled");
        }
    }
     
    centro = determinaCentro();
    zoom = determinaZoom();

    $('#passo'+passo).addClass("green");

    mapa = new google.maps.Map(
        document.getElementById('mapa'),
        {
            zoom: zoom,
            center: centro,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        } 
    );
})


function determinaCentro(){
    centro = $('#centro').val();
    if(centro==""){
        centro="0,0";
    }
    coordenadas = centro.split(",");
    return new google.maps.LatLng(coordenadas[0],coordenadas[1]);
}

function determinaZoom(){
    zoom = $('#zoom').val();
    if(zoom=="" || zoom < 2){
        return 2;
    }
    return parseInt(zoom);
}

function defineCor(){
    caracteres = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
    var cor = "#"; 
    for (i=0;i<6;i++){ 
       cor += caracteres[Math.floor(Math.random() * (caracteres.length))]; 
    }
    return cor;
}

function identificaPoligono(){
    poligono = $('#poligono_da_consulta').val();
    poligono_plotavel = preparaLinestring(poligono);
    plota_poligono(poligono_plotavel);
}

function preparaLinestring(linestring){
    var trajetoria_plotavel = new Array ();
    
    pontos = linestring.split(" ");
    for(var p in pontos){
        ponto = pontos[p].split(",");
        latitude = ponto[0];
        longitude = ponto[1];
        trajetoria_plotavel.push(new google.maps.LatLng(latitude,longitude));
    }
    return trajetoria_plotavel;
}