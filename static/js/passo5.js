var objeto_inicio_referencial;
var objeto_trajeto_referencial;

var objetos_inicio_par = new Array();
var objetos_pares = new Array();

var objetos_inicio_subtrajetoria = new Array();
var objetos_subtrajetorias = new Array();

$('document').ready(function(){

	$('.ui.accordion').accordion();
    $('.ui.checkbox').checkbox();
    $('.standard.test.modal').modal('show');
    
    identificaPoligono();

    subTrajetoriasCompletas();
    trajetoriaReferencial();

	pares = $('#pares').val().split(";");
	
	for(var p in pares){
		par = pares[p].split("_");
		cor = defineCor();

		posicao = objetos_pares.length;
		
        objetos_inicio_par.push(new Array());
        objetos_pares.push(new Array());

		for(var t in par){
			trajeto = par[t];
			if(trajeto.length > 0){
				linestring = preparaLinestring(trajeto);
				plotaLinestring(linestring,cor,posicao);
			}
		}
	}
    
    $('.botao').click(function(){
        id = $(this).attr("id");
        destacaPar(-1);
        destacaSubtrajetoria(id);
    });

    $('.botao_trecho').click(function(){
        id = $(this).attr("id");
        destacaSubtrajetoria(-1);
        destacaPar(id);
    });

    $('.referencial').change(function(){
        if($(this).is(':checked')){
            objeto_inicio_referencial.setVisible(true);
            objeto_trajeto_referencial.setOptions({strokeOpacity:0.6});
        }else{
            objeto_inicio_referencial.setVisible(false);
            objeto_trajeto_referencial.setOptions({strokeOpacity:0});
        }
    })

})

function preparaLinestring(linestring){
	var trajetoria_plotavel = new Array ();
    
    pontos = linestring.split(" ");
    for(var p in pontos){
        ponto = pontos[p].split(",");
        latitude = ponto[0];
        longitude = ponto[1];
        trajetoria_plotavel.push(new google.maps.LatLng(latitude,longitude));
    }
    return trajetoria_plotavel;

}

function plotaLinestring(linestring,cor,posicao){
	
    pontoInicial(linestring[0],'','par',posicao);

	var trajeto = new google.maps.Polyline({
        path: linestring,
        geodesic: false,
        strokeColor: cor,
        strokeOpacity: 0,
        strokeWeight: 2
    });

    trajeto.setMap(mapa);

    objetos_pares[posicao].push(trajeto);
}

function subTrajetoriasCompletas(){
    subtrajetorias = $('#subtrajetorias_completas').val().split(";");
    ids = $('#ids').val().split(";");

    for(var s in subtrajetorias){
        if(subtrajetorias[s].length>0){
            subtrajetoria = subtrajetorias[s];

            subtrajetoria_plotavel = preparaLinestring(subtrajetoria);
            cor = defineCor();

            pontoInicial(subtrajetoria_plotavel[0],ids[s],'sub');

            var subtrajeto = new google.maps.Polyline({
                path: subtrajetoria_plotavel,
                geodesic: false,
                strokeColor: cor,
                strokeOpacity: 0.1,
                strokeWeight: 2
            });

            objetos_subtrajetorias.push(subtrajeto);
            subtrajeto.setMap(mapa);
        }
    }
}

function pontoInicial(coordenadas,titulo,tipo,posicao){
    posicao = typeof posicao !== 'undefined' ? posicao : 0;
    tipo = typeof tipo !== 'undefined' ? tipo : '';

    var ponto_inicial = new google.maps.Marker({
        position: coordenadas,
        title: titulo,
        icon: 'http://maps.gstatic.com/intl/en_ALL/mapfiles/dd-start.png'
    });

    if(tipo == 'sub'){
        objetos_inicio_subtrajetoria.push(ponto_inicial);
    }
    if(tipo == 'par'){
        objetos_inicio_par[posicao].push(ponto_inicial);
    }
    if(tipo == 'ref'){
        objeto_inicio_referencial = ponto_inicial;
    }

    ponto_inicial.setVisible(false);
    ponto_inicial.setMap(mapa);
}

function destacaSubtrajetoria(id){
    
    for(var s in objetos_subtrajetorias){
        if(s == id){
            objetos_subtrajetorias[s].setOptions({strokeOpacity: 1});
            objetos_inicio_subtrajetoria[s].setVisible(true);
        }else{
            objetos_subtrajetorias[s].setOptions({strokeOpacity: 0.1});
            objetos_inicio_subtrajetoria[s].setVisible(false);
        }
    }
}

function destacaPar(id){
    for(var p in objetos_pares){
        if(p == id){
            for(var o in objetos_pares[p]){
                objetos_pares[p][o].setOptions({strokeOpacity: 1})
                objetos_inicio_par[p][o].setVisible(true);
            }
        }
        else{
            for(var o in objetos_pares[p]){
                objetos_pares[p][o].setOptions({strokeOpacity: 0})
                objetos_inicio_par[p][o].setVisible(false);
            }
        }
    }
}

function trajetoriaReferencial(){
    referencial_plotavel = preparaLinestring($('#trajetoria_referencial').val());

    pontoInicial(referencial_plotavel[0],'Trajetória Referencial','ref');

    var referencial = new google.maps.Polyline({
        path: referencial_plotavel,
        geodesic: false,
        strokeColor: defineCor(),
        strokeOpacity: 0,
        strokeWeight: 2
    });

    objeto_trajeto_referencial = referencial;
    referencial.setMap(mapa);
}