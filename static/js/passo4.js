
$(document).ready(function(){
    range();
    identificaPoligono();

    trajetorias = $('#trajetorias_encontradas').val().split(";");
    for(var t in trajetorias){
        if(trajetorias[t].length > 0){
            trajeto_plotavel = preparaLinestring(trajetorias[t]);
            cor = defineCor();
            plota_trajetoria(trajeto_plotavel,cor,-1,1);
        }
    }

    var markerCluster = new MarkerClusterer(mapa,marcadores);
})

function range(){
    
    $('#tempo').ready(function(){
        valor_atual = $('#tempo').val();
        $('#tempo_atual').html(valor_atual+" %");
    })

    $('#comprimento').ready(function(){
        valor_atual = $('#comprimento').val();
        $('#comprimento_atual').html(valor_atual+" %");
    })

    $('#forma').ready(function(){
        valor_atual = $('#forma').val();
        $('#forma_atual').html(valor_atual+" %");
    })

    if($('#trajetorias_encontradas').val() != 'False'){

        $('#tempo').change(function(){
            valor_atual = $('#tempo').val();
            $('#tempo_atual').html(valor_atual+" %");
        })

        $('#comprimento').change(function(){
            valor_atual = $('#comprimento').val();
            $('#comprimento_atual').html(valor_atual+" %");
        })

        $('#forma').change(function(){
            valor_atual = $('#forma').val();
            $('#forma_atual').html(valor_atual+" %");
        })

        $('#and').click(function(){
            $(this).removeClass("disabled");
            $('#or').addClass("disabled");
            document.getElementById("and_radio").checked = true;
        })

        $('#or').click(function(){
            $(this).removeClass("disabled");
            $('#and').addClass("disabled");
            document.getElementById("or_radio").checked = true;
        })
    }

}