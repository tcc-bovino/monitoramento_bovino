var marcadores = new Array();

$(document).ready(function(){
    
    identificaTrajetorias();
    var markerCluster = new MarkerClusterer(mapa,marcadores);
    var nome_bovino = $('#textarea_nome_bovino').val().split(';');
    var brinco = $('#textarea_brinco').val().split(';');
    var id_trajetoria = $('#textarea_id_trajetoria').val().split(';');
    var data_inicio_fim = $('#textarea_data_inicio_fim').val().split(';');
    var comprimento = $('#textarea_comprimento').val().split(';');
    var tempo = $('#textarea_tempo').val().split(';');
    var velocidade_minima = $('#textarea_velocidade_minima').val().split(';');
    var velocidade_maxima = $('#textarea_velocidade_maxima').val().split(';');
    var velocidade_media = $('#textarea_velocidade_media').val().split(';');
    var quantidade_pontos = $('#textarea_qtd_pontos').val().split(';');

    for (i in marcadores) {
        mostraInfoWindow(marcadores,i, nome_bovino, brinco, id_trajetoria, data_inicio_fim, comprimento, tempo, velocidade_minima, velocidade_maxima, velocidade_media, quantidade_pontos);
    }


    var creator = new PolygonCreator(mapa);

    $('#limpa').click(function(){
        creator.destroy();
        creator=null;
        
        $("#submitPasso3").addClass("disabled");
        coordenadasDoPoligono = new Array();
        $('#painelcoordenadas').val(null);
        
        creator = new PolygonCreator(mapa);
    });

})

function insereCoordenadasNoInput(coordenadas){
    for(var par in coordenadas){
        $('#painelcoordenadas').val($('#painelcoordenadas').val()+coordenadas[par]);
    }
    $('#submitPasso3').removeClass('disabled');
}

function identificaTrajetorias(){
    trajetorias = $('#trajetorias_encontradas').val().split(";");

    for(var i in trajetorias){
        trajetoria = trajetorias[i];

        if(trajetoria.length>0){
            trajetoria_plotavel = identificaCoordenadas(trajetoria);
            plotaTrajetoria(trajetoria_plotavel);
        }
    }
}

function identificaCoordenadas(trajetoria){
    var trajetoria_plotavel = new Array ();
    pontos = trajetoria.split(" ");

    for(var p in pontos){
        ponto = pontos[p].split(",");
        latitude = ponto[0];
        longitude = ponto[1];

        trajetoria_plotavel.push(new google.maps.LatLng(latitude,longitude));
    }
    
    return trajetoria_plotavel;
}


function plotaTrajetoria(trajetoria,id){
    cor = defineCor();
    extremo(trajetoria[0]);

    if(typeof trajetoria)
    var trajeto = new google.maps.Polyline({
        path: trajetoria,
        geodesic: false,
        strokeColor: cor,
        strokeOpacity: 1.0,
        strokeWeight: 2
    });
    trajeto.setMap(mapa);
}

function extremo(ponto){
    var ponto = new google.maps.Marker({
      position: ponto,
      map: mapa,
      icon: 'http://maps.gstatic.com/intl/en_ALL/mapfiles/dd-start.png'
    });
    marcadores.push(ponto);
}

function mostraInfoWindow(marcadores,i, nome_bovino, brinco, id_trajetoria, data_inicio_fim, comprimento, tempo, velocidade_minima, velocidade_maxima, velocidade_media, quantidade_pontos) {
    var conteudo = setValoresInfoWindow(nome_bovino[i], brinco[i], id_trajetoria[i], data_inicio_fim[i], comprimento[i], tempo[i], velocidade_minima[i], velocidade_maxima[i], velocidade_media[i], quantidade_pontos[i]);
    var ponto = marcadores[i];

    // adiciona infowindow
    var infowindow = new google.maps.InfoWindow({
            content: conteudo,
            maxWidth: 250
    });

    // Procedimento que mostra a Info Window através de um click no marcador
    google.maps.event.addListener(ponto, 'click', function() {
            infowindow.open(mapa,ponto);
    });
}

function setValoresInfoWindow(nome_bovino, brinco, id_trajetoria, data_inicio_fim, comprimento, tempo, velocidade_minima, velocidade_maxima, velocidade_media, quantidade_pontos) {
    var conteudo = '<div id="iw-container">' +
    					'<div class="iw-title"><b>Nome do bovino: '+ nome_bovino + '</b></div>'  +
    					'<div class="iw-title"><b>Brinco: '+ brinco + '</b></div>' +
    					'<div class="iw-content">' +
    					  // '<img src="../../images/cow-1.png" alt="vaca" class="imagem-ifowindow">' +
    					  '<ul>'+
    					  '<li>ID da trajetória: ' + id_trajetoria + '</li>' +
    					  '<li>Data: '+ data_inicio_fim + '</li>' +
                          '<li>Comprimento da trajetória: '+ comprimento +' km </li>' +
                          '<li>Tempo de duração: '+ tempo +' </li>' +
    					  '<li>Velocidade mínima da trajetória: '+  velocidade_minima + ' km/h </li>' +
    					  '<li>Velocidade máxima da trajetória: '+ velocidade_maxima + ' km/h </li>' +
    					  '<li>Velocidade média da trajetória: '+ velocidade_media + ' km/h </li>' +
    					  '<li>Quantidade de pontos da trajetória: '+ quantidade_pontos +'</li>' +
    					  '</ul>' +
    					'</div>' +
    					'<div class="iw-bottom-gradient"></div>' +
    				  '</div>';

     // Se o conteúdo da infowindow não ultrapassar a altura máxima definida, então o gradiente é removido.
    // if($('.iw-content').height() < 140){
    if($('.iw-content').height() < 80){
      $('.iw-bottom-gradient').css({display: 'none'});
    }

    return conteudo;
}