from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from django.conf import settings
from django.conf.urls.static import static
from app_bovino import views
from app_bovino.views import InseminacaoCreateView, InseminacaoUpdateView, ConfirmarInseminacaUpdateView, GestacaoUpdateView, LoteBovinoUpdateView, CategoriaBovinoUpdateView, StatusReprodutivoBovinoUpdateView, MovimentacaoBovinoListView

urlpatterns = patterns('',
    # url(r'^home/$', TemplateView.as_view(template_name='app_bovino/index.html'), name='index'),
    url(r'^painel_controle/$', views.painelControle, name='painel_controle'),
    url(r'^configuraces_app_bovino/$', views.configuracoes, name='configuracoes_app_bovino'),
    url(r'^notificacoes/$', views.notificacoes, name='notificacoes'),

    #cadastrar
    url(r'^cadastrar_pessoa/$', views.cadastrarPessoa, name='cadastrar_pessoa'),
    url(r'^cadastrar_raca/$', views.cadastrarRaca, name='cadastrar_raca'),
    url(r'^cadastrar_lote/$', views.cadastrarLote, name='cadastrar_lote'),
    url(r'^cadastrar_categoria/$', views.cadastrarCategoria, name='cadastrar_categoria'),
    url(r'^cadastrar_status_reprodutivo/$', views.cadastrarStatusReprodutivo, name='cadastrar_status_reprodutivo'),
    url(r'^cadastrar_bovino/$', views.cadastrarBovino, name='cadastrar_bovino'),
    # url(r'^cadastrar_bovino_lote/$', views.cadastrarBovinoLote, name='cadastrar_bovino_lote'),
    # url(r'^cadastrar_bovino_categoria/$', views.cadastrarBovinoCategoria, name='cadastrar_bovino_categoria'),
    # url(r'^cadastrar_bovino_status_reprodutivo/$', views.cadastrarBovinoStatusReprodutivo, name='cadastrar_bovino_status_reprodutivo'),
    url(r'^cadastrar_esperma/$', views.cadastrarEsperma, name='cadastrar_esperma'),
    # url(r'^cadastrar_inseminacao/$', views.cadastrarInseminacao, name='cadastrar_inseminacao'),
    url(r'^cadastrar_inseminacao/$', InseminacaoCreateView.as_view(), name='cadastrar_inseminacao'),
    url(r'^cadastrar_gestacao/$', views.cadastrarGestacao, name='cadastrar_gestacao'),
    url(r'^cadastrar_descarte/$', views.cadastrarDescarte, name='cadastrar_descarte'),

    #listar
    url(r'^listar_pessoa/$', views.listarPessoa, name='cadastrar_pessoa'),
    url(r'^listar_raca/$', views.listarRaca, name='listar_raca'),
    url(r'^listar_lote/$', views.listarLote, name='listar_lote'),
    url(r'^listar_categoria/$', views.listarCategoria, name='listar_categoria'),
    url(r'^listar_status_reprodutivo/$', views.listarStatusReprodutivo, name='listar_status_reprodutivo'),
    url(r'^listar_bovino/$', views.listarBovino, name='listar_bovino'),
    url(r'^listar_lote_bovinos/$', views.LoteBovinosListView, name='listar_lote_bovinos'),
    url(r'^listar_categoria_bovinos/$', views.CategoriaBovinosListView, name='listar_categoria_bovinos'),
    url(r'^listar_status_reprodutivo_bovinos/$', views.StatusReprodutivoBovinosListView, name='listar_status_reprodutivo_bovinos'),
    url(r'^listar_esperma/$', views.listarEsperma, name='listar_esperma'),
    url(r'^listar_inseminacao/$', views.listarInseminacao, name='listar_inseminacao'),
    url(r'^listar_gestacao/$', views.listarGestacao, name='listar_gestacao'),
    url(r'^listar_descarte/$', views.listarDescarte, name='listar_descarte'),
    url(r'^listar_movimentacao_bovinos/$', MovimentacaoBovinoListView.as_view(), name='listar_movimentacao_bovinos'),

    #visualizar
    url(r'^pessoa/(?P<id>\d+)/visualizar_pessoa/$', views.visualizarPessoa, name='visualizar_pessoa'),
    url(r'^raca/(?P<id>\d+)/visualizar_raca/$', views.visualizarRaca, name='visualizar_raca'),
    url(r'^lote/(?P<id>\d+)/visualizar_lote/$', views.visualizarLote, name='visualizar_lote'),
    url(r'^categoria/(?P<id>\d+)/visualizar_categoria/$', views.visualizarCategoria, name='visualizar_categoria'),
    url(r'^status_reprodutivo/(?P<id>\d+)/visualizar_status_reprodutivo/$', views.visualizarStatusReprodutivo, name='visualizar_status_reprodutivo'),
    url(r'^bovino/(?P<id>\d+)/visualizar_bovino/$', views.visualizarBovino, name='visualizar_bovino'),
    url(r'^lote/(?P<pk>\d+)/visualizar_lote_bovino/$', views.LoteBovinoDetailView, name='visualizar_lote_bovino'),
    url(r'^categoria/(?P<pk>\d+)/visualizar_categoria_bovino/$', views.CategoriaBovinoDetailView, name='visualizar_categoria_bovino'),
    url(r'^status_reprodutivo/(?P<pk>\d+)/visualizar_status_reprodutivo_bovino/$', views.StatusReprodutivoBovinoDetailView, name='visualizar_status_reprodutivo_bovino'),
    url(r'^esperma/(?P<id>\d+)/visualizar_esperma/$', views.visualizarEsperma, name='visualizar_esperma'),
    url(r'^inseminacao/(?P<id>\d+)/visualizar_inseminacao/$', views.visualizarInseminacao, name='visualizar_inseminacao'),
    url(r'^gestacao/(?P<id>\d+)/visualizar_gestacao/$', views.visualizarGestacao, name='visualizar_gestacao'),
    url(r'^descarte/(?P<id>\d+)/visualizar_descarte/$', views.visualizarDescarte, name='visualizar_descarte'),

    #editar
    url(r'^pessoa/(?P<id>\d+)/editar_pessoa/$', views.editarPessoa, name='editar_pessoa'),
    url(r'^racao/(?P<id>\d+)/editar_raca/$', views.editarRaca, name='editar_raca'),
    url(r'^lote/(?P<id>\d+)/editar_lote/$', views.editarLote, name='editar_lote'),
    url(r'^categoria/(?P<id>\d+)/editar_categoria/$', views.editarCategoria, name='editar_categoria'),
    url(r'^status_reprodutivo/(?P<id>\d+)/editar_status_reprodutivo/$', views.editarStatusReprodutivo, name='editar_status_reprodutivo'),
    url(r'^bovino/(?P<id>\d+)/editar_bovino/$', views.editarBovino, name='editar_bovino'),
    url(r'^lote/(?P<pk>\d+)/editar_lote_bovino/$', LoteBovinoUpdateView.as_view(), name='editar_lote_bovino'),
    url(r'^categoria/(?P<pk>\d+)/editar_categoria_bovino/$', CategoriaBovinoUpdateView.as_view(), name='editar_categoria_bovino'),
    url(r'^status_reprodutivo/(?P<pk>\d+)/editar_status_reprodutivo_bovino/$', StatusReprodutivoBovinoUpdateView.as_view(), name='editar_status_reprodutivo_bovino'),
    url(r'^esperma/(?P<id>\d+)/editar_esperma/$', views.editarEsperma, name='editar_esperma'),
    url(r'^inseminacao/(?P<pk>\d+)/editar_inseminacao/$', InseminacaoUpdateView.as_view(), name='editar_inseminacao'),
    # url(r'^inseminacao/confirmar_inseminacao/$', ConfirmarInseminacaUpdateView.as_view(), name='confirmar_inseminacao'),
    url(r'^inseminacao/(?P<inseminacao_id>\d+)/confirmar_inseminacao/$', ConfirmarInseminacaUpdateView.as_view(), name='confirmar_inseminacao'),
    url(r'^gestacao/(?P<pk>\d+)/editar_gestacao/$', GestacaoUpdateView.as_view(), name='editar_gestacao'),
    url(r'^descarte/(?P<id>\d+)/editar_descarte/$', views.editarDescarte, name='editar_descarte'),

    #excluir
    url(r'^pessoa/(?P<id>\d+)/excluir_pessoa/$', views.excluirPessoa, name='excluir_pessoa'),
    url(r'^racao/(?P<id>\d+)/excluir_raca/$', views.excluirRaca, name='excluir_raca'),
    url(r'^lote/(?P<id>\d+)/excluir_lote/$', views.excluirLote, name='excluir_lote'),
    url(r'^categoria/(?P<id>\d+)/excluir_categoria/$', views.excluirCategoria, name='excluir_categoria'),
    url(r'^status_reprodutivo/(?P<id>\d+)/excluir_status_reprodutivo/$', views.excluirStatusReprodutivo, name='excluir_status_reprodutivo'),
    url(r'^bovino/(?P<id>\d+)/excluir_bovino/$', views.excluirBovino, name='excluir_bovino'),
    # url(r'^lote/(?P<id>\d+)/excluir_bovino_lote/$', views.excluirBovinoLote, name='excluir_lote'),
    # url(r'^categoria/(?P<id>\d+)/excluir_bovino_categoria/$', views.excluirBovinoCategoria, name='excluir_bovino_categoria'),
    # url(r'^status_reprodutivo/(?P<id>\d+)/excluir_bovino_status_reprodutivo/$', views.excluirBovinoStatusReprodutivo, name='excluir_bovino_status_reprodutivo'),
    url(r'^esperma/(?P<id>\d+)/excluir_esperma/$', views.excluirEsperma, name='excluir_esperma'),
    url(r'^inseminacao/(?P<id>\d+)/excluir_inseminacao/$', views.excluirInseminacao, name='excluir_inseminacao'),
    url(r'^gestacao/(?P<id>\d+)/excluir_gestacao/$', views.excluirGestacao, name='excluir_gestacao'),
    url(r'^descarte/(?P<id>\d+)/excluir_descarte/$', views.excluirDescarte, name='excluir_descarte'),
)

if not settings.DEBUG:
      urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
      urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
