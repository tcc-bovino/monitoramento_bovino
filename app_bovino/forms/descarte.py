from django import forms
from app_bovino.models import Descarte

class FormDescarte(forms.ModelForm):
    class Meta:
        model = Descarte
        fields = "__all__"
