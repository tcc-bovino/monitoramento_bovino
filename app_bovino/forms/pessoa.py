from django import forms
from app_bovino.models import Pessoa

class FormPessoa(forms.ModelForm):
    class Meta:
        model = Pessoa
        fields = "__all__"
