from django import forms
from app_bovino.models import StatusReprodutivo

class FormStatusReprodutivo(forms.ModelForm):
    class Meta:
        model = StatusReprodutivo
        fields = "__all__"
