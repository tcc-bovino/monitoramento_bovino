from django.contrib.auth.models import User
from registration.forms import RegistrationFormUniqueEmail

class UserForm (RegistrationFormUniqueEmail):
     class Meta:
         model = User
         fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2')
