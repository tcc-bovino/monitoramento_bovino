#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from app_bovino.models.inseminacao import Inseminacao, STATUS_INSEMINACAO
from app_bovino.models.bovino import Bovino
from app_bovino.models.esperma import Esperma
from app_bovino.models.pessoa import Pessoa

class FormInseminacao(forms.ModelForm):
    class Meta:
        model = Inseminacao
        fields = ('vaca', 'touro', 'data_inseminacao', 'inseminador', 'observacao')
        # exclude = ('status_inseminacao')

    def __init__(self, *args, **kwargs):
        super(FormInseminacao, self).__init__(*args, **kwargs)

        vacas = Bovino.objects.filter(is_femea=True)
        touros = Esperma.objects.all()

        self.fields['vaca'].widget = forms.Select(
            choices=vacas.values_list('id', 'nome'),
            attrs={'class': 'ui dropdown'})

        self.fields['touro'].widget = forms.Select(
            choices=touros.values_list('id', 'nome'),
            attrs={'class': 'ui dropdown'})

        # self.fields['status_inseminacao'].widget = forms.Select(
        #     choices=STATUS_INSEMINACAO,
        #     attrs={'class': 'ui dropdown'})


class FormInseminacaoEditar(forms.ModelForm):
    class Meta:
        model = Inseminacao
        fields = ('vaca', 'touro', 'data_inseminacao', 'inseminador', 'data_prevista_cio', 'data_cio', 'observacao')
        # exclude = ('status_inseminacao')

    def __init__(self, *args, **kwargs):
        super(FormInseminacaoEditar, self).__init__(*args, **kwargs)

        vacas = Bovino.objects.filter(is_femea=True)
        touros = Esperma.objects.all()

        self.fields['vaca'].widget = forms.Select(
            choices=vacas.values_list('id', 'nome'),
            attrs={'class': 'ui dropdown'})

        self.fields['touro'].widget = forms.Select(
            choices=touros.values_list('id', 'nome'),
            attrs={'class': 'ui dropdown'})

        # self.fields['status_inseminacao'].widget = forms.Select(
        #     choices=STATUS_INSEMINACAO,
        #     attrs={'class': 'ui dropdown'})










