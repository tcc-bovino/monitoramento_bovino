from django import forms
from app_bovino.models import Gestacao

class FormGestacaoDiagnostico(forms.ModelForm):
    class Meta:
        model = Gestacao
        fields = ('data_diagnostico_gestacao',)


