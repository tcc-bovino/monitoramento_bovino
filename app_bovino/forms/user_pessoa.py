from django import forms
from betterforms.multiform import MultiModelForm
from app_bovino.forms import FormPessoa, UserForm

class UserMultiForm(MultiModelForm):
    form_classes = {
        'user': UserForm,
        'pessoa': FormPessoa,
    }

    def save(self, commit=True):
        objects = super(UserMultiForm, self).save(commit=False)

        if commit:
            user = objects['user']
            user.save()
            pessoa = objects['pessoa']
            pessoa.user = user
            pessoa.save()

        return objects