# -*- coding: utf-8 -*-
from django import template

register = template.Library()

@register.filter(name='converte_metros_para_quilometros')
def converte_metros_para_quilometros(distancia):
    return distancia * 3.6

@register.filter(name='converte_quilometros_para_metros')
def converte_quilometros_para_metros(distancia):
    return distancia / 3.6
