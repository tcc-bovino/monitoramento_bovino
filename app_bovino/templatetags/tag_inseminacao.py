# -*- coding: utf-8 -*-
from django import template
from datetime import datetime, timedelta
from app_bovino.models import StatusReprodutivo, Bovino

register = template.Library()

STATUS_REPRODUTIVO_CONFIRMAR = 'A confirmar'
STATUS_REPRODUTIVO_PRENHA = 'Prenha'
PREVISAO_CIO = 21
PREVISAO_SECAR = 60
HOJE = datetime.now()

@register.filter(name='calcula_data_prevista_diagnostico_gestacao')
def calcula_data_prevista_diagnostico_gestacao(data_inseminacao):
    status_reprodutivo_confirmar = StatusReprodutivo.objects.filter(status_reprodutivo=STATUS_REPRODUTIVO_CONFIRMAR)[0]

    if status_reprodutivo_confirmar.periodo_duracao_previsto:
        data_prevista_diagnostico_gestacao = data_inseminacao + timedelta(status_reprodutivo_confirmar.periodo_duracao_previsto)
    else:
        data_prevista_diagnostico_gestacao = data_inseminacao + timedelta(status_reprodutivo_confirmar.periodo_duracao_padrao)

    return data_prevista_diagnostico_gestacao

@register.filter(name='calcula_data_prevista_cio')
def calcula_data_prevista_cio(data_inseminacao):
        return data_inseminacao + timedelta(PREVISAO_CIO)

@register.filter(name='calcula_data_prevista_parto')
def calcula_data_prevista_parto(data_inseminacao):
    status_reprodutivo_prenha = StatusReprodutivo.objects.get(status_reprodutivo=STATUS_REPRODUTIVO_PRENHA)

    if status_reprodutivo_prenha.periodo_duracao_previsto:
        data_prevista_parto = data_inseminacao + timedelta(status_reprodutivo_prenha.periodo_duracao_previsto)
    else:
        data_prevista_parto = data_inseminacao + timedelta(status_reprodutivo_prenha.periodo_duracao_padrao)

    return data_prevista_parto


@register.filter(name='calcula_data_prevista_secar')
def calcula_data_prevista_secar(data_inseminacao):
    data_prevista_parto = calcula_data_prevista_parto(data_inseminacao)
    return data_prevista_parto - timedelta(PREVISAO_SECAR)

@register.filter(name='get_data_ultimo_parto')
def get_data_ultimo_parto(bovino):
    partos = Bovino.objects.filter(mae=bovino.id).order_by('data_nascimento')

    if partos:
        data_ultimo_parto = partos[len(partos) - 1].data_nascimento
    return data_ultimo_parto


@register.filter(name='calcula_dias_pos_ultimo_parto')
def calcula_dias_pos_ultimo_parto(bovino):
    data_ultimo_parto = get_data_ultimo_parto(bovino)
    dias_pos_ultimo_parto = int((HOJE.date() - data_ultimo_parto).days)
    return dias_pos_ultimo_parto

@register.filter(name='calcula_del')
def calcula_del(bovino):
    data_ultimo_parto = get_data_ultimo_parto(bovino)
    del_dias_em_lactacao = int((HOJE.date() - data_ultimo_parto).days)
    return del_dias_em_lactacao

@register.filter(name='calcula_ipc')
def calcula_ipc(bovino, data_inseminacao):
    data_ultimo_parto = get_data_ultimo_parto(bovino)
    ipc = int((data_inseminacao - data_ultimo_parto).days)
    return ipc


