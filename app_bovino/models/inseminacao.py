#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.db import models
from bovino import Bovino
from esperma import Esperma
# from pessoa import Pessoa


STATUS_INSEMINACAO = (
    (False, 'A confirmar'),
    (True, 'Prenha'),
)


class Inseminacao(models.Model):
    #chaves
    vaca = models.ForeignKey(Bovino, blank=False, help_text='Vaca:')
    data_inseminacao = models.DateField(blank=True, help_text='Data de inseminação:')
    data = models.DateTimeField(auto_now_add=True)
    touro = models.ForeignKey(Esperma,  blank=False, help_text='Touro:')
    inseminador = models.CharField(max_length=255, blank=False, help_text='Inseminador:')
    observacao = models.CharField(max_length=255, blank=True, help_text='Observação:')
    status_inseminacao = models.BooleanField(default=False, blank=False, choices=STATUS_INSEMINACAO,
                                    help_text='Status da inseminação')
    data_prevista_cio = models.DateField(blank=True, null=True, help_text='Data prevista do cio:')
    data_cio = models.DateField(blank=True, null=True, help_text='Data do cio:')


    def __unicode__(self):
        return '%s %s e %s' %(self.data_inseminacao, self.vaca.nome, self.touro.nome)
        # return self.vaca.nome