#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from raca import Raca
from categoria import Categoria

class Esperma(models.Model):
    nome = models.CharField(max_length=128, blank=True, help_text='Nome:')
    codigo_compra = models.CharField(max_length=20, blank=True, help_text='Código da compra:')
    data_nascimento = models.DateField(blank=False, help_text='Data de nascimento:')
    pai = models.CharField(max_length=20, blank=False, help_text='Pai:')
    mae = models.CharField(max_length=20, blank=False, help_text='Mãe:')
    raca = models.ForeignKey(Raca, blank=False, help_text='Raça:')
    #categoria = models.ManyToManyField(Categoria,through='BovinoCategoria', blank=False, help_text='Categoria')
    quantidade_comprada = models.IntegerField(blank=False, help_text='Quantidade comprada:')
    quantidade_estoque = models.IntegerField(help_text='Quantidade em estoque:')
    distribuidora = models.CharField(max_length=255, blank=True, null=True, help_text='Distribuidora:')
    codigo = models.IntegerField(blank=True, null=True, help_text='Código:')


    def __unicode__(self):
        return self.codigo_compra
