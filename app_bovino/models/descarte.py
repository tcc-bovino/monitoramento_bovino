#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from bovino import Bovino

class Descarte(models.Model):
    bovino = models.ForeignKey(Bovino,blank=False, help_text='Animal:')
    data = models.DateTimeField(auto_now_add=True)
    observacao = models.CharField(max_length=255, blank=True, help_text='Observação:')


    def __unicode__(self):
        return self.bovino
