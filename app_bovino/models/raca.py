#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

class Raca(models.Model):
    raca = models.CharField(max_length=128,  blank=False, help_text='Raça:')
    descricao = models.CharField(max_length=255,  blank=True, help_text='Descrição:')

    def __unicode__(self):
        return self.raca
