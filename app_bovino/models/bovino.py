#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from raca import Raca
from lote import Lote
from categoria import Categoria
from status_reprodutivo import StatusReprodutivo
from esperma import Esperma
# from pessoa import Pessoa

SEXOS = (
    (False, 'Macho'),
    (True, 'Fêmea'),
)

class Bovino(models.Model):
    nome = models.CharField(max_length=128, blank=True, help_text='Nome:')
    brinco = models.CharField(max_length=128, blank=False, help_text='Nº do brinco:')
    is_femea = models.BooleanField(default=False, blank=False, choices=SEXOS, help_text='Sexo:')
    raca = models.ForeignKey(Raca, blank=True, help_text='Raça:' )

    #many to many - utilizar opção through apara apontar qual é a model associativa
    lote = models.ManyToManyField(Lote, through='BovinoLote', blank=True, help_text='Lote:')
    categoria = models.ManyToManyField(Categoria, through='BovinoCategoria', blank=True, help_text='Categoria')
    status_reprodutivo = models.ManyToManyField(StatusReprodutivo, through='BovinoStatusReprodutivo', blank=True, help_text='Status reprodutivo:')

    data_nascimento = models.DateField(blank=False, help_text='Data de nascimento:')
    hora_nascimento = models.TimeField(blank=True, null=True, help_text='Hora do nascimento')
    data_falecimento = models.DateField(blank=True, null=True, help_text='Data do falecimento')
    hota_falecimento = models.TimeField(blank=True, null=True, help_text='Hora do falecimento')
    data_cadastro = models.DateTimeField(auto_now_add=True)

    mae = models.ForeignKey('self', related_name='+',  blank=True, null=True, help_text='Mãe:')
    pai = models.ForeignKey(Esperma, blank=True, null=True, help_text='Esperma:')
    # pai = models.ForeignKey('self', related_name='+', blank=True, null=True, help_text='Pai:')

    imagem = models.ImageField(upload_to='imagem_bovino', blank=True, null=True, help_text='Foto:')

    # pessoa = models.ForeignKey(Pessoa, blank=False, null=False)
    # pessoa = models.ForeignKey(User, blank=True, null=True)

    def __unicode__(self):
        return self.nome
