#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from bovino import Bovino
from categoria import Categoria

class BovinoCategoria(models.Model):
    bovino = models.ForeignKey(Bovino, on_delete=models.CASCADE)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    data = models.DateTimeField(auto_now_add=True)

    class Meta:
        auto_created = True
