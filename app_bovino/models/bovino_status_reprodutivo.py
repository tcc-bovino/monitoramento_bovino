#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from bovino import Bovino
from status_reprodutivo import StatusReprodutivo

class BovinoStatusReprodutivo(models.Model):
    bovino = models.ForeignKey(Bovino, on_delete=models.CASCADE)
    status_reprodutivo = models.ForeignKey(StatusReprodutivo, on_delete=models.CASCADE)
    data_inicio = models.DateField(blank=True, null=True, help_text='Data inicial do status reprodutivo:')
    data_fim = models.DateField(blank=True, null=True, help_text='Data final do status reprodutivo:')
    data = models.DateTimeField(auto_now_add=True)

    class Meta:
        auto_created = True
