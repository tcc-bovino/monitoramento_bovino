#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from app_bovino.forms import FormEsperma
from app_bovino.models import Esperma

#@login_required
def cadastrarEsperma(request):
    # um HTTP post?
    if request.method == 'POST':
        form = FormEsperma(request.POST)

        # forms é valido
        if form.is_valid():
            esperma = form.save(commit=True)
            return HttpResponseRedirect(reverse(visualizarEsperma, args=(esperma.id,)))
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = FormEsperma()

    return render(request, 'app_bovino/cadastrar/esperma.html', {'form': form})

def visualizarEsperma(request, id):
    esperma = Esperma.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/esperma.html', {'esperma': esperma})

def editarEsperma(request, id):
    if request.method == 'POST':
        if id:
            esperma = Esperma.objects.get(id=id)
            form = FormEsperma(request.POST,instance=esperma)
        else:
            form = FormEsperma(request.POST)
        if form.is_valid():
            esperma = form.save()
            return HttpResponseRedirect(reverse(visualizarEsperma, args=(esperma.id,)))
    else:
        if id:
            esperma = Esperma.objects.get(id=id)
            form = FormEsperma(instance=esperma)

    return render(request, 'app_bovino/editar/esperma.html', {'form':form, 'esperma': esperma})

def excluirEsperma(request, id):
    esperma = Esperma.objects.get(id=id)

    if request.method == 'POST':
        esperma.delete()
        return listarEsperma(request)

    return render(request, 'app_bovino/excluir/esperma.html', {'esperma': esperma})

def listarEsperma(request):
    espermas = Esperma.objects.all()
    return render(request, 'app_bovino/listar/esperma.html', {'espermas': espermas})
