#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from app_bovino.forms import FormDescarte
from app_bovino.models import Descarte

#@login_required
def cadastrarDescarte(request):
    # um HTTP post?
    if request.method == 'POST':
        form = FormDescarte(request.POST)

        # forms é valido
        if form.is_valid():
            descarte = form.save(commit=True)
            return HttpResponseRedirect(reverse(visualizarDescarte, args=(descarte.id,)))
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = FormDescarte()

    return render(request, 'app_bovino/cadastrar/descarte.html', {'form': form})

def visualizarDescarte(request, id):
    descarte = Descarte.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/descarte.html', {'descarte': descarte})

def editarDescarte(request, id):
    if request.method == 'POST':
        if id:
            descarte = Descarte.objects.get(id=id)
            form = FormDescarte(request.POST,instance=descarte)
        else:
            form = FormDescarte(request.POST)
        if form.is_valid():
            descarte = form.save()
            return HttpResponseRedirect(reverse(visualizarDescarte, args=(descarte.id,)))
    else:
        if id:
            descarte = Descarte.objects.get(id=id)
            form = FormDescarte(instance=descarte)

    return render(request, 'app_bovino/editar/descarte.html', {'form':form, 'descarte': descarte})

def excluirDescarte(request, id):
    descarte = Descarte.objects.get(id=id)

    if request.method == 'POST':
        descarte.delete()
        return listarDescarte(request)

    return render(request, 'app_bovino/excluir/descarte.html', {'descarte': descarte})

def listarDescarte(request):
    descartes = Descarte.objects.all()
    return render(request, 'app_bovino/listar/descarte.html', {'descartes': descartes})
