#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from app_bovino.forms import FormLote
from app_bovino.models import Lote


#@login_required
def cadastrarLote(request):
    if request.method == 'POST':
        form = FormLote(request.POST)

        # forms é valido
        if form.is_valid():
            lote = form.save(commit=True)
            return HttpResponseRedirect(reverse(visualizarLote, args=(lote.id,)))
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = FormLote()

    return render(request, 'app_bovino/cadastrar/lote.html', {'form': form})

def visualizarLote(request, id):
    lote = Lote.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/lote.html', {'lote': lote})

def editarLote(request, id):
    if request.method == 'POST':
        if id:
            lote = Lote.objects.get(id=id)
            form = FormLote(request.POST,instance=lote)
        else:
            form = FormLote(request.POST)
        if form.is_valid():
            lote = form.save()
            return HttpResponseRedirect(reverse(visualizarLote, args=(lote.id,)))
    else:
        if id:
            lote = Lote.objects.get(id=id)
            form = FormLote(instance=lote)

    return render(request, 'app_bovino/editar/lote.html', {'form':form, 'lote': lote})

def excluirLote(request, id):
    lote = Lote.objects.get(id=id)

    if request.method == 'POST':
        lote.delete()
        return listarLote(request)

    return render(request, 'app_bovino/excluir/lote.html', {'lote': lote})

def listarLote(request):
    lotes = Lote.objects.all()
    return render(request, 'app_bovino/listar/lote.html', {'lotes': lotes})
