#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import UpdateView

from app_bovino.forms import FormGestacao
from app_bovino.models import Gestacao, Bovino, Lote, StatusReprodutivo

STATUS_REPRODUTIVO_POS_PARTO = 'Pós parto'
LOTE_SECA = 'Seca'

#@login_required
def cadastrarGestacao(request):
    if request.method == 'POST':
        form = FormGestacao(request.POST)

        # forms é valido
        if form.is_valid():
            gestacao = form.save(commit=True)
            return HttpResponseRedirect(reverse(visualizarGestacao, args=(gestacao.id,)))
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = FormGestacao()

    return render(request, 'app_bovino/cadastrar/gestacao.html', {'form': form})

def visualizarGestacao(request, id):
    gestacao = Gestacao.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/gestacao.html', {'gestacao': gestacao})

class GestacaoUpdateView(UpdateView):
    template_name = 'app_bovino/editar/gestacao.html'
    model = Gestacao
    form_class = FormGestacao

    def form_valid(self, form):
        gestacao = form.save(commit=False)

        bovino = Bovino.objects.get(id=gestacao.inseminacao.vaca.id)
        if gestacao.data_parto:
            bovino.status_reprodutivo = StatusReprodutivo.objects.filter(
                status_reprodutivo=STATUS_REPRODUTIVO_POS_PARTO)
        if gestacao.data_secar:
            bovino.lote = Lote.objects.filter(lote=LOTE_SECA)
        bovino.save()

        gestacao.save()

        return super(GestacaoUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('visualizar_gestacao', kwargs={'id': self.get_object().id})

def excluirGestacao(request, id):
    gestacao = Gestacao.objects.get(id=id)

    if request.method == 'POST':
        gestacao.delete()
        return listarGestacao(request)

    return render(request, 'app_bovino/excluir/gestacao.html', {'gestacao': gestacao})

def listarGestacao(request):
    gestacoes = Gestacao.objects.filter(inseminacao__status_inseminacao=True)
    return render(request, 'app_bovino/listar/gestacao.html', {'gestacoes': gestacoes})
