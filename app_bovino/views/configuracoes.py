#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect

#@login_required
def configuracoes(request):
    data = []
    keys = ['model', 'url_cadastrar', 'url_listar', 'url_visualizar', 'url_editar']
    model = ['Categoria', 'Lote', 'Raça', 'Status Reprodutivo']
    url_cadastrar = ['cadastrar_categoria', 'cadastrar_lote', 'cadastrar_raca', 'cadastrar_status_reprodutivo']
    url_listar = ['listar_categoria', 'listar_lote', 'listar_raca', 'listar_status_reprodutivo']
    url_visualizar = ['visualizar_categoria', 'visualizar_lote', 'visualizar_raca', 'visualizar_status_reprodutivo']
    url_editar = ['editar_categoria', 'editar_lote', 'editar_raca', 'editar_status_reprodutivo']

    tamanho = len(model)
    for i in range(tamanho):
        temp = [model[i], url_cadastrar[i], url_listar[i], url_visualizar[i], url_editar[i]]
        data.append(dict(zip(keys, temp)))
    return render(request, 'app_bovino/configuracoes.html', {'cards': data})
