#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.utils import timezone
from datetime import datetime
from django.db import connection
from django.views.generic.base import View
from django.db.models import Max, Min, Avg, Count
from trajesim.models import Pontos, Trajetorias
from app_bovino.templatetags.tag_datetime import *

class MovimentacaoBovinoListView(View):
    def get(self, request):
        inicio = datetime(2017, 01, 01).date()
        hoje = datetime.now().date()
        data_inicio = inicio.strftime("%Y-%m-%d")
        # data_inicio = hoje.strftime("%Y-%m-%d")
        data_fim = hoje.strftime("%Y-%m-%d")

        # datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        movimentacao_bovinos = PesquisaPeriodo(data_inicio, data_fim)


        return render(request, 'app_bovino/listar/movimentacao_bovino.html', {'objects': movimentacao_bovinos})

    def post(self, request):
        data_inicio = datetime.strptime(request.POST.get('data_inicio'), '%Y-%m-%d').date().strftime("%Y-%m-%d")
        data_fim = datetime.strptime(request.POST.get('data_fim'), '%Y-%m-%d').date().strftime("%Y-%m-%d")

        movimentacao_bovinos = PesquisaPeriodo(data_inicio, data_fim)
        # keys = ['quantidade_pontos', 'velocidade_minima', 'comprimento', '_state', 'id_trajetoria', 'bovino_id', 'tempo',
        #  'velocidade_media', 'velocidade_maxima', 'data_hora_inicio', 'data_hora_fim', 'descricao', 'trajetoria']

        return render(request, 'app_bovino/listar/movimentacao_bovino.html', {'objects': movimentacao_bovinos})

def PesquisaPeriodo(data_inicio, data_fim):
    trajetorias = VerificaPeriodo(data_inicio, data_fim)
    resultado_tejatorias = []


    for trajetoria in trajetorias:
        pontos = Pontos.objects.filter(id_trajetoria=trajetoria.id_trajetoria, data_hora_ponto__range=(data_inicio, data_fim)).order_by('data_hora_ponto')

        dados_trajetoria = {
            'velocidade_minima': getVelocidadeMinima(pontos),
            'velocidade_maxima': getVelocidadeMaxima(pontos),
            'velocidade_media_intervalo': getVelocidadeMedia(pontos),
            'quantidade_pontos': getQuantidadePontos(pontos),
            'tempo_duracao': getTempoDuracao(pontos),
            'comprimento_intervalo': getComprimento(pontos)
        }

        traj = trajetoria.__dict__
        traj = traj.copy()
        traj.update(dados_trajetoria)

        resultado_tejatorias.append(traj)

    return resultado_tejatorias


def VerificaPeriodo(data_inicio, data_fim):
    trajetorias = []
    ids_trajetorias = []


    resultado = Pontos.objects.filter(data_hora_ponto__range=(data_inicio, data_fim))

    for linha in resultado:
        if linha.id_trajetoria_id not in ids_trajetorias:
            trajetoria = Trajetorias.objects.get(id_trajetoria=linha.id_trajetoria_id)
            ids_trajetorias.append(linha.id_trajetoria_id)
            trajetorias.append(trajetoria)

    return trajetorias

def getVelocidadeMinima(pontos):
    return pontos.aggregate(Min('velocidade'))

def getVelocidadeMaxima(pontos):
    return pontos.aggregate(Max('velocidade'))

def getVelocidadeMedia(pontos):
    '''retorna em m/s para converter para km/h basta multiplicar por 3,6
       1m/s = 3,6 km/h
    '''
    comprimento_metros = getComprimento(pontos)
    tempo_segundos = calcula_tempo_total_segundos(getTempoDuracao(pontos))

    return comprimento_metros/tempo_segundos


def getQuantidadePontos(pontos):
    return pontos.aggregate(Count('id_ponto'))

def getComprimento(pontos):
    ''' por padrão o postgis retorna o st_length em metros'''
    linestring = makeLineString(pontos)

    query = "SELECT ST_Length(" + linestring + ")"
    return my_custom_sql(query)[0]

def getTempoDuracao(pontos):
    # TODO: atributo tempo da tabela trajetória está em segundos
    pontos = pontos.order_by('data_hora_ponto')
    return (pontos[len(pontos)-1].data_hora_ponto - pontos[0].data_hora_ponto)

def makeLineString(pontos):
    novo_pontos = []
    for ponto in pontos:
        novo_pontos.append(('%s %s' % (ponto.ponto[0], ponto.ponto[1])))

    linestring = ','.join(novo_pontos)
    return "ST_GeomFromText('LINESTRING("+ linestring + ")', 4326)"

def my_custom_sql(query):
    with connection.cursor() as cursor:
        cursor.execute(query)
        row = cursor.fetchone()
    return row

#     -- YX ::: ID Bovino, ID Trajetória, Descricao da Trajetória, Tempo de Duração, Comprimento, Pontos, Velocidade Média, Data e Hora Inicial, Data e Hora Final
# SELECT t.id_trajetoria, count(p.id_ponto) as qtd_pontos, (t.data_hora_fim - t.data_hora_inicio) as tempo_duracao, ST_Length(t.trajetoria_yx)*3.6 as comprimento_km, min(p.velocidade) as velocidade_min_km, max(p.velocidade) as velocidade_max_km,
# (ST_Length(t.trajetoria_yx)/EXTRACT(EPOCH FROM (t.data_hora_fim-t.data_hora_inicio)))*3.6 as vm_kmh
# FROM trajesim_trajetorias as t inner join trajesim_pontos as p on t.id_trajetoria = p.id_trajetoria_id group by t.id_trajetoria order by t.id_trajetoria

# -- tempo de duração
# select (data_hora_fim - data_hora_inicio), EXTRACT(EPOCH FROM (data_hora_fim-data_hora_inicio))from trajesim_trajetorias

# -- tempo em segundos
# select EXTRACT(EPOCH FROM (data_hora_fim-data_hora_inicio)) from trajesim_trajetorias


# -- comprimento metros
# select ST_Length(trajetoria) as m, ST_Length(trajetoria)*3.6 as km from trajesim_trajetorias

# -- quantidade de pontos
# select id_trajetoria_id, count(id_ponto) from trajesim_pontos group by id_trajetoria_id order by id_trajetoria_id

# -- velocidade foi cadastrada em km/h
# -- velocidade mínima
# select min(velocidade) from trajesim_pontos group by id_trajetoria_id order by id_trajetoria_id

# -- velocidade máxima
# select max(velocidade) from trajesim_pontos group by id_trajetoria_id order by id_trajetoria_id

# -- velocidade média
# select (ST_Length(trajetoria)/EXTRACT(EPOCH FROM (data_hora_fim-data_hora_inicio))) as m_s, (ST_Length(trajetoria)/EXTRACT(EPOCH FROM (data_hora_fim-data_hora_inicio)))*3.6 as k_h from trajesim_trajetorias

