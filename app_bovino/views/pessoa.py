#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from app_bovino.forms import FormPessoa
from app_bovino.models import Pessoa

#@login_required
def cadastrarPessoa(request):
    # um HTTP post?
    if request.method == 'POST':
        form = FormPessoa(request.POST)

        # forms é valido
        if form.is_valid():
            pessoa = form.save(commit=True)
            return HttpResponseRedirect(reverse(visualizarPessoa, args=(pessoa.id,)))
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = FormPessoa()

    return render(request, 'app_bovino/cadastrar/pessoa.html', {'form': form})

def visualizarPessoa(request, id):
    pessoa = Pessoa.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/pessoa.html', {'pessoa': pessoa})

def editarPessoa(request, id):
    if request.method == 'POST':
        if id:
            pessoa = Pessoa.objects.get(id=id)
            form = FormPessoa(request.POST,instance=pessoa)
        else:
            form = FormPessoa(request.POST)
        if form.is_valid():
            pessoa = form.save()
            return HttpResponseRedirect(reverse(visualizarPessoa, args=(pessoa.id,)))
    else:
        if id:
            pessoa = Pessoa.objects.get(id=id)
            form = FormPessoa(instance=pessoa)

    return render(request, 'app_bovino/editar/pessoa.html', {'form':form, 'pessoa': pessoa})

def excluirPessoa(request, id):
    pessoa = Pessoa.objects.get(id=id)

    if request.method == 'POST':
        pessoa.delete()
        return listarPessoa(request)

    return render(request, 'app_bovino/excluir/pessoa.html', {'pessoa': pessoa})

def listarPessoa(request):
    pessoas = Pessoa.objects.all()
    return render(request, 'app_bovino/listar/pessoa.html', {'pessoas': pessoas})
