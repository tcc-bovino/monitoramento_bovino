# -*- coding: utf-8 -*-

from django.contrib.gis.db import models
from trajesim.models.trajetorias import Trajetorias
from trajesim.models.consultas import Consultas
from django.contrib.postgres.fields import ArrayField

class SubTrajetoria(models.Model):
    id_sub_trajetoria = models.AutoField(primary_key=True)
    id_trajetoria = models.ForeignKey(Trajetorias)
    id_consulta = models.ForeignKey(Consultas)
    sub_trajetoria = models.LineStringField()
    sub_comprimento = models.FloatField()
    sub_tempo = models.FloatField()
    sub_dif_azimute = ArrayField(ArrayField(models.CharField(max_length=100000)))
    
    objects = models.GeoManager()