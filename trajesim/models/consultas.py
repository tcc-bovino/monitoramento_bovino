# -*- coding: utf-8 -*-

from django.contrib.gis.db import models
from trajesim.models.trajetorias import Trajetorias
from django.contrib.postgres.fields import ArrayField
from datetime import datetime


class Consultas(models.Model):
    id_consulta = models.AutoField(primary_key=True)
    id_trajetoria_referencial = models.ForeignKey(Trajetorias)
    data_consulta = models.DateTimeField(default=datetime.now(),blank=True)
    data_pesquisa_inicio = models.DateField()
    data_pesquisa_fim = models.DateField()
    poligono = models.PolygonField()
    trajref_dif_azimute = ArrayField(ArrayField(models.CharField(max_length=100000)))

    objects = models.GeoManager()

    def __unicode__(self):
        return '%s\n %s' %(self.poligono, self.data_consulta)