# -*- coding: utf-8 -*-

from trajesim.models import Trajetorias

def format_linestring(linestring):
    """ 
        Formata a linestring, recebendo-a no formatdo do PostGIS 'LINESTRING(1 2, 2 3, 3 4)' e retorna em formato de lista com 
        tuplas de coordenadas -> [[1,2],[2,3],[3,4]] 
    """
    formated_points = []
    points_init = linestring.find("(")+1 # Verifica a posição onde iniciam as coordenadas
    traject = linestring[points_init:-1] # delimita a string na posição onde inicia e termina as coordenadas
    for point in traject.split(","): # quebra o trajeto nas coordenadas
        coordinates = point.split(" ") # quebra a coordenada em latitude e longitude
        lat,lon = float(coordinates[0]),float(coordinates[1]) # modifica o tipo para float
        formated_points.append([lat,lon]) # adiciona a latitude e longitude como uma tupla na lista de pontos
    return formated_points

def determina_tempo_decorrido(id_trajetoria, comprimento):
    
    trajetoria = Trajetorias.objects.get(id_trajetoria=int(id_trajetoria))
    velocidade_media = trajetoria.velocidade_media
    tempo_horas = float(comprimento/velocidade_media)
    return tempo_horas
    
