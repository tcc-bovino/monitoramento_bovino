# -*- coding: utf-8 -*-

from trajesim.models import *

def ConverteParaConsulta(string,geometry='polygon'):
    '''
        A função recebe uma string que contém as coordenadas que forma um polígono. Como essa
        string como sua origem a API do Google, o formato dela é do tipo: "(a,b)(c,d)(e,f)(g,h)...(n,m)"

        Partindo desse formato, a função retorna essa string modifica e adaptada para ser usada como
        filtro em uma consulta no banco de dados geográfico. O PostGIS trabalha com um formato de coordenadas da 
        seguinte maneira: "POLYGON((a b,c d,e f,g h, ... ,m n))"

        >>> converteParaPostGIS('(5,5)(4,3)(8,1)(9,-2)(5,5)')
        "POLYGON((5 5,4 3,8 1,9 -2,5 5))"
    '''

    lista_de_coordenadas = string[1:-1].split(")(")

    if(geometry.upper() == "POLYGON"):
        lista_de_coordenadas.append(lista_de_coordenadas[0])

    string_convertida = ",".join([par.replace(",","") for par in lista_de_coordenadas])
    

    return "%s((%s))" %(geometry.upper(),string_convertida) 

def ConverteParaPlotagem(string):
    ''' 
        A função recebe uma string no derivada da API do Google Maps ou de uma consulta no banco de dados.
        Essa string vem em formatos referentes à suas derivações. 
        
        API Google Maps: "(a,b)(c,d)(e,f)(g,h)...(n,m)"
                            ou
        Consulta no banco: "LINESTRING((a b,c d,e f,g h, ... ,m n))"

        A função retorna uma string em um formato que facilita a manipulação e plotagem posteriormente pelo
        Javascript. Os códigos Javascripts usados na aplicação recebem trajetos e poligonos no formato:
        "a,b c,d e,f g,h ... m,n"

        >>> converteParaAPIGoogle('LINESTRING(1 3,4 5,8 7,10 12)')
        "1 3,4 5,8 7,10 12" 
    '''
    coordenadas = []

    if string.find(")(") == -1: #se for derivado do POSTGIS
        inicio = string.find("(")
        
        for ponto in string[inicio:].split(","):
            ponto = ponto.lstrip()
            ponto = ponto.replace(" ",",")

            coordenadas.append(ponto)

    else: #se for derivado da API Google Maps
        for ponto in string.split(")("):   
            ponto = ponto.replace(" ","")

            coordenadas.append(ponto)

    stringFormatada = " ".join(coordenadas)
    # retorna a string com os parenteses removidos dos extremos
    return stringFormatada.replace("(","").replace(")","")

def SeparaTrajetorias(trajetorias):
    trajetorias_separadas_em_lines = []

    for linha in trajetorias:
        
        if linha['trajetoria'][:4] == 'MULT':
            inicio = (linha['trajetoria'].find('('))+2
            trajetorias = linha['trajetoria'][inicio:-2].split("),(")
            
            for trajetoria in trajetorias:
                linestring ={}
                linestring['id_trajetoria'] = linha['id']
                linestring['trajetoria']='LINESTRING('+'%s)' %(trajetoria)
                trajetorias_separadas_em_lines.append(linestring)
        else:
            inicio = (linha['trajetoria'].find('('))+1
            trajetoria = linha['trajetoria'][inicio:-1]
            
            linestring = {}
            linestring['id_trajetoria'] = linha['id']
            linestring['trajetoria']='LINESTRING('+'%s)' %(trajetoria)
            trajetorias_separadas_em_lines.append(linestring)
            
    
    return trajetorias_separadas_em_lines

def AjustaPlotagem(trajetorias):
    trajetos_plotar = []
    for linha in trajetorias:
        trajetoria = ConverteParaPlotagem(str(linha['trajetoria']))
        trajetos_plotar.append(trajetoria)

    return trajetos_plotar

def AjustaData(data):
    '''
    Ajusta data pata o formato aceito pelo Django para salvar objetos Date e DateTime

    >>>AjustaData('1/1/2000')
    '1-1-2000'
    '''
    return   data.replace('/','-')

def AgrupaTrajetos(trajetos):
    linestring_plotavel = ''
    for trajeto in trajetos:
        for coordenadas in trajeto:
            lat,lon = str(coordenadas[0]),str(coordenadas[1])
            linestring_plotavel += ','.join([lat,lon])
            linestring_plotavel += ' '
        linestring_plotavel = linestring_plotavel[:-1]+'_'
    return linestring_plotavel[:-1]


def ConverteParaLinestring(string, geometry='linestring'):
    '''
        A função recebe uma string que contém as coordenadas que forma um polígono. Como essa
        string como sua origem a API do Google, o formato dela é do tipo: "(a,b)(c,d)(e,f)(g,h)...(n,m)"

        Partindo desse formato, a função retorna essa string modifica e adaptada para ser usada como
        filtro em uma consulta no banco de dados geográfico. O PostGIS trabalha com um formato de coordenadas da
        seguinte maneira: "POLYGON((a b,c d,e f,g h, ... ,m n))"

        >>> converteParaPostGIS('(5,5)(4,3)(8,1)(9,-2)(5,5)')
        "POLYGON((5 5,4 3,8 1,9 -2,5 5))"
    '''

    lista_de_coordenadas = string[1:-1].split(")(")

    if (geometry.upper() == "LINESTRING"):
        lista_de_coordenadas.append(lista_de_coordenadas[0])

    # string_convertida = ",".join([par.replace(",", "") for par in lista_de_coordenadas])
    string_convertida = ",".join([par.replace(",", ";") for par in lista_de_coordenadas])
    string_convertida = string_convertida.replace(" ", ",")
    string_convertida = string_convertida.replace(";", " ")

    return "'%s(%s)'" % (geometry.upper(), string_convertida)
