# -*- coding: utf-8 -*-

import numpy
from math import *
from trajesim.funcoes.tratamento_dados.azimute import *

def correlacao(lista1,lista2):
    if(len(lista1) >=2):
        return abs(numpy.corrcoef(lista1, lista2)[0, 1])*100
    else:
        return 100.0

def calcula_correlacao_de_comprimentos_diferentes(trajetoria_maior,trajetoria_menor,referencial_maior=False):
    num_diferencas_trajetoria_maior = len(trajetoria_maior['diferencas'])
    num_diferencas_trajetoria_menor = len(trajetoria_menor['diferencas'])
    
    correlacoes = []
    for i,x in enumerate(trajetoria_maior['diferencas']):
        if(i+num_diferencas_trajetoria_menor <= num_diferencas_trajetoria_maior):
            trecho_trajetoria_maior = trajetoria_maior['diferencas'][i:i+num_diferencas_trajetoria_menor]
            pontos_do_trecho = trajetoria_maior['pontos'][i:i+len(trajetoria_menor['pontos'])]
            similaridade = correlacao(trajetoria_menor['diferencas'],trecho_trajetoria_maior)
            
            identificador = '%s - %s.%s' %(trajetoria_menor['id'],trajetoria_maior['id'],i+1)
            
            trajetos = [trajetoria_menor['pontos'],pontos_do_trecho]
            
            correlacoes.append({
                'id': identificador, 
                'trajetos': trajetos, 
                'similaridade_forma': similaridade
            })
            # id: identifica as trajetorias ou trechos de trajetorias que foram comparadas
            # trajetos: as coordenadas/pontos das trajetorias comparadas
            # similaridade: o percentual entre elas
    
    return correlacoes

def similaridade_forma(referencial,subtraj):
    ''' 
        O que ela recebe:
            trajetoria_referencial (REFERENCIAL) = {
                'id_trajetoria',
                format_linestring('trajetoria'), 
                'trajref_dif_azimute'
            }
            
            subtrajetoria = {
                'id_sub_trajetoria', 
                format_linestring('sub_trajetoria'),
                'sub_dif_azimute'
            }
            
        Essa é a função essencial do operador de FORMA. Ela recebe duas listas compostas por diferenças de azimutes de
        duas trajetórias DISTINTAS e com comprimento maior ou igual a 2 '''
    
    trajetoria_referencial = {
            'id': referencial.id_sub_trajetoria,
            'pontos':format_linestring(str(referencial.sub_trajetoria)),
            'diferencas': list(map(float, referencial.sub_dif_azimute))
        }

    subtrajetoria = {
            'id': subtraj.id_sub_trajetoria,
            'pontos': format_linestring(str(subtraj.sub_trajetoria).split(";")[1]),
            'diferencas': list(map(float, subtraj.sub_dif_azimute))
        }

    if len(trajetoria_referencial['diferencas']) == len(subtrajetoria['diferencas']):
        similaridade = correlacao(trajetoria_referencial['diferencas'],subtrajetoria['diferencas'])
        
        return [{
                'id': subtrajetoria['id'], 
                'trajetos': [subtrajetoria['pontos'],trajetoria_referencial['pontos']], 
                'similaridade_forma': similaridade
            }]
        
    elif len(trajetoria_referencial['diferencas']) >= len(subtrajetoria['diferencas']):
        return calcula_correlacao_de_comprimentos_diferentes(trajetoria_referencial,subtrajetoria,True)
    
    else:
        return calcula_correlacao_de_comprimentos_diferentes(subtrajetoria,trajetoria_referencial)

    

