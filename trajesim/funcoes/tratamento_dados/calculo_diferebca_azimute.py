def azimuth(coordenadas):
    ''' Formula que calcula o azimute entre dois pontos dispostos no plano terrestre '''
    
    lat1,lon1,lat2,lon2 = coordenadas[0],coordenadas[1],coordenadas[2],coordenadas[3]
    phi1 = radians(lat1)
    phi2 = radians(lat2)
    delta = radians(lon2 - lon1)
    
    y = sin(delta) * cos(phi2)
    x = cos(phi1) * sin(phi2) - sin(phi1) * cos(phi2) * cos(delta)
    
    teta = atan2(x,y)
    
    return (degrees(teta) + 360) % 360