# -*- coding: utf-8 -*-

from trajesim.models import Consultas, Trajetorias
from trajesim.funcoes.tratamento_dados.tratamento_dados import *
from datetime import datetime

def SaveConsulta(poligono,trajetoria_referencial, data_inicio, data_fim):

    consulta = Consultas()
    consulta.id_trajetoria_referencial = Trajetorias.objects.get(id_trajetoria=int(trajetoria_referencial))
    consulta.data_pesquisa_inicio = datetime.strptime('%s' %(AjustaData(data_inicio)), '%d-%m-%Y').date()
    consulta.data_pesquisa_fim = datetime.strptime('%s' %(AjustaData(data_fim)), '%d-%m-%Y').date()
    consulta.poligono = (poligono + ', 4326)')
    consulta.save()

    return {'id_consulta':consulta.id_consulta}


