# -*- coding: utf-8 -*-

from trajesim.models import SubTrajetoria, Consultas, Trajetorias
from trajesim.funcoes.tratamento_dados.calculo_comprimento import *
from trajesim.funcoes.tratamento_dados.azimute import result_dif_azimutes
from trajesim.funcoes.tratamento_dados.calculo_tempo import determina_tempo_decorrido

def SalvaSubtrajetorias(id_consulta, trajetorias):

    for trajetoria in trajetorias:
        
        subtrajetoria =SubTrajetoria()
        subtrajetoria.id_trajetoria = Trajetorias.objects.get(id_trajetoria=int(trajetoria['id_trajetoria']))
        subtrajetoria.id_consulta = Consultas.objects.get(id_consulta=int(id_consulta['id_consulta']))
        subtrajetoria.sub_trajetoria = (trajetoria['trajetoria'] + ', 4326)')
        subtrajetoria.sub_comprimento = comprimento_total(trajetoria['trajetoria'])
        subtrajetoria.sub_tempo = determina_tempo_decorrido(trajetoria['id_trajetoria'],subtrajetoria.sub_comprimento)
        subtrajetoria.sub_dif_azimute = ((result_dif_azimutes(trajetoria['trajetoria']))['diferencas'])
        subtrajetoria.save()
