from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from django.conf import settings
from django.conf.urls.static import static
from trajesim.views import *

urlpatterns = patterns('',
    # url(r'^$', TemplateView.as_view(template_name='trajesim/index.html'), name='index'),
    url(r'^home/$', Passo1Views.as_view(), name='index'),

    url(r'^$', Passo1Views.as_view(), name='passo1'),
    url(r'^trajetorias/1/', Passo1Views.as_view(), name='passo1'),
    url(r'^trajetorias/2/', Passo2Views.as_view(), name='passo2'),
    url(r'^trajetorias/3/', Passo3Views.as_view(), name='passo3'),
    url(r'^trajetorias/4/', Passo4Views.as_view(), name='passo4'),
    url(r'^trajetorias/5/', Passo5Views.as_view(), name='passo5'),
    # url(r'^uploader/', UploaderIndex.as_view(), name='uploader'),
    # url(r'^process/', UploaderProcess.as_view(), name='process'),
    # url(r'^remover/', UploaderRemove.as_view(), name='remover'),
    # url(r'^inserir/', InsercaoDeDados.as_view(), name='inserir'),
)

if not settings.DEBUG:
      urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
      urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
