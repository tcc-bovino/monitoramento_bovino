#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from django.shortcuts import render
from django.views.generic.base import View
from django.http import HttpResponseRedirect

from monitoramento_bovino.settings import *
from trajesim.forms import UploadForm
from trajesim.models import Upload

from trajesim.funcoes.uploader.upload import *
from trajesim.funcoes.uploader.importador import *

class UploaderIndex(View):

	def get(self,request):
		form = UploadForm(request.POST, request.FILES)
		files = Upload.objects.all()

		return render(request, 'trajesim/uploader.html', {
				'form': form,
				'files': files
			})
		
class UploaderProcess(View):

	def post(self,request):
		form = UploadForm(request.POST, request.FILES)
		
		try:
			up = processaUpload(request.FILES['arquivo'])
			if up == True:
				return HttpResponseRedirect('/uploader/')
			else:
				return render(request, 'trajesim/uploader_processo.html', {
						'mensagem': up
					})
		except:
			return render(request, 'trajesim/uploader_processo.html', {
					'mensagem': "Você deve inserir um documento nos formatos propostos (txt, plt)"
				})

class UploaderRemove(View):

	def get(self,request):

		id = request.GET.get('id')
		if id == 'all':
			objetos = Upload.objects.all()
		else:
			objetos = [Upload.objects.get(pk=id)]
		
		for objeto in objetos:
			arquivo_removido = objeto.titulo
			objeto.delete()

			for arquivo in os.listdir(MEDIA_ROOT):
				if arquivo == arquivo_removido:
					os.remove('%s%s' %(MEDIA_ROOT,arquivo))
					pass

		return HttpResponseRedirect('/uploader')

class InsercaoDeDados(View):

	def get(self,request):
		importa_dados()
		return HttpResponseRedirect('/remover?id=all')