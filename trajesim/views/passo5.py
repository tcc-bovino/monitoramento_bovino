# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.base import View
from trajesim.funcoes.tratamento_dados.similaridades import *
from trajesim.funcoes.pesquisa.pesquisa_informacoes import *

class Passo5Views(View):
    def get(self, request):
        return HttpResponseRedirect('/trajetorias/1/')

    def post(self, request):

        id_consulta = request.session["id_consulta"]
        globais = request.session["globais"]
        subtrajetoria_referencial = request.session['subtrajetoria_referencial']

        similaridade_tempo = request.POST.get("tempo")
        similaridade_comprimento = request.POST.get("comprimento")
        similaridade_forma = request.POST.get("forma")
        operador = request.POST.get("operador")

        resultado_similaridade, trajref_plotavel = VerificaSimilaridade(
            similaridade_tempo,
            similaridade_comprimento,
            id_consulta,
            subtrajetoria_referencial,
            'and',
            similaridade_forma
        )

        referencial = ReferencialDetalhes(subtrajetoria_referencial)
        similaridades = {'tempo': similaridade_tempo, 'comprimento': similaridade_comprimento, 'forma': similaridade_forma}

        return render(request, 'trajesim/passo5.html', {
                'passo': 5,
                'resultado_similaridade':resultado_similaridade,
                'trajref_plotavel':trajref_plotavel,
                'global': globais,
                'similaridades': similaridades,
                'referencial': referencial
            })