# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic.base import View

from trajesim.funcoes.pesquisa.pesquisa_informacoes import PesquisaPeriodo


class Passo1Views(View):

    def get(self, request):
        try:
            del request.session['globais','trajetoria_referencial','periodo','subtrajetoria_referencial','id_consulta']
        
        except:
            pass

        return render(request, 'trajesim/passo1.html', {'passo': 1})