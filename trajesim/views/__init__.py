# -*- coding: utf-8 -*-
from uploader import UploaderIndex, UploaderProcess, UploaderRemove, InsercaoDeDados

from passo1 import Passo1Views
from passo2 import Passo2Views
from passo3 import Passo3Views
from passo4 import Passo4Views
from passo5 import Passo5Views