# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.base import View
from trajesim.funcoes.tratamento_dados.tratamento_dados import *
from trajesim.funcoes.auxiliares_web.auxiliares_JavaScript import *
from trajesim.funcoes.persistencia_banco.salva_consulta import *
from trajesim.funcoes.persistencia_banco.salva_subtrajetorias import *
from trajesim.funcoes.pesquisa.pesquisa_informacoes import *

class Passo3Views(View):
    
    def get(self, request):
        if 'subtrajetoria_referencial' in request.session:
            try:
                del request.session['subtrajetoria_referencial']

                return render(request, 'trajesim/passo3.html', {
                    'passo': 3,
                    'global': request.session['globais']
                })
            except:
                return HttpResponseRedirect('/trajetorias/1/')
        else:
            return HttpResponseRedirect('/trajetorias/1/')

    def post(self, request):
        periodo = request.session['periodo']
        poligono = request.POST.get('coordenadas')

        poligono_consulta = ConverteParaConsulta(poligono,'polygon')
        poligono_plotavel = ConverteParaPlotagem(poligono)
        
        centro_e_zoom = IdentificaCentroZoom(poligono_plotavel)

        pesquisa = ProcessaConsulta(poligono_consulta)
        
        if pesquisa == False:
            trajetos_subconsulta = []
            subtrajetos = False
        else:
            trajetos_subconsulta = pesquisa[1]

            id_consulta = SaveConsulta(
                poligono_consulta,
                1, #referencial
                periodo['data_inicial'], 
                periodo['data_final']
            ) 

            request.session['id_consulta'] = id_consulta
            SalvaSubtrajetorias(id_consulta,trajetos_subconsulta)
            subtrajetos = BuscaSubTrajetorias(id_consulta['id_consulta'])

        globais = {'poligono': poligono_plotavel,'centro': centro_e_zoom[0],'zoom': centro_e_zoom[1],'trajetos': subtrajetos}
        request.session['globais'] = globais

        return render(request, 'trajesim/passo3.html', {
                'passo': 3,
                'global': globais
            })