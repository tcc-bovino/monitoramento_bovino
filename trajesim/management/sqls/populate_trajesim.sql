-- trajetórias
\i trajetorias/trajetorias_betinha.sql
\i trajetorias/trajetorias_dairy.sql

-- update trajetorias
\i scripts_update/trajetorias_update.sql
\i scripts_update/trajetorias_update_coordenadas.sql

-- pontos
\i pontos/betinha/pontos_trajetorias1_betinha.sql
\i pontos/betinha/pontos_trajetorias2_betinha.sql
\i pontos/betinha/pontos_trajetorias3_betinha.sql

\i pontos/dayri/pontos_trajetorias1_dayri.sql
\i pontos/dayri/pontos_trajetorias2_dayri.sql
\i pontos/dayri/pontos_trajetorias3_dayri.sql
\i pontos/dayri/pontos_trajetorias4_dayri.sql
\i pontos/dayri/pontos_trajetorias5_dayri.sql

-- update pontos
\i scripts_update/pontos_update_coordenadas.sql
