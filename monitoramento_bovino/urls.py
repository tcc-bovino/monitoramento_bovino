from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('trajesim.urls')),
    url(r'^app_bovino/', include('app_bovino.urls')),
    url(r'^app_geografico/', include('app_geografico.urls')),

    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^accounts/login/', TemplateView.as_view(template_name='registration/login.html'), name='login'),
    # url(r'^/accounts/register/complete', ),

]
