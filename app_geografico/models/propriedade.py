#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from django.db import models
from django.contrib.gis.db import models

class Propriedade(models.Model):
    descricao = models.CharField(max_length=255,  blank=True, help_text='Descrição:')
    delimitacao = models.PolygonField()
    objects = models.GeoManager()

    def __unicode__(self):
        return self.descricao
