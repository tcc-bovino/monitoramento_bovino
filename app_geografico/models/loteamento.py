#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from django.db import models
from django.contrib.gis.db import models
from propriedade import Propriedade

class Loteamento(models.Model):
    descricao = models.CharField(max_length=255,  blank=True, help_text='Descrição:')
    delimitacao = models.PolygonField()
    propriedade = models.ForeignKey(Propriedade)
    is_ocupado = models.BooleanField()
    is_construido = models.BooleanField()

    # objects = models.GeoManager()

    def __unicode__(self):
        return self.descricao
