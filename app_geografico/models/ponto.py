#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from django.db import models
from django.contrib.gis.db import models
from trajetoria import Trajetoria

class Ponto(models.Model):
    ponto = models.PointField()
    id_trajetoria = models.ForeignKey(Trajetoria)
    data_hora = models.DateTimeField(blank=False, help_text='Data e hora da coleta:')
    acelerometro_x = models.FloatField()
    acelerometro_y = models.FloatField()
    acelerometro_z = models.FloatField()
    velocidade = models.FloatField()

    # objects = models.GeoManager()

    def __unicode__(self):
        return self.ponto
