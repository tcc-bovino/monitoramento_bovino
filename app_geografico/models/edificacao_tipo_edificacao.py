#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

from app_geografico.models.tipo_edificacao import TipoEdificacao
from edificacao import Edificacao


class EdificacaoTipoEdificacao(models.Model):
    edificacao = models.ForeignKey(Edificacao, on_delete=models.CASCADE)
    tipo_edificacao = models.ForeignKey(TipoEdificacao, on_delete=models.CASCADE)
    data = models.DateTimeField(auto_now_add=True)
    em_vigencia = models.BooleanField()

    class Meta:
        auto_created = True
