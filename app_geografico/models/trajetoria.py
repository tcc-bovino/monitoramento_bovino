#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from django.db import models
from django.contrib.gis.db import models
from app_bovino.models.bovino import Bovino

class Trajetoria(models.Model):
    bovino = models.ForeignKey(Bovino, blank=True, help_text='Bovino:')
    descricao = models.CharField(max_length=128, blank=True, help_text='Descrição:')
    trajetoria = models.LineStringField()
    data_hora_inicio = models.DateTimeField(blank=False, help_text='Data e hora inicial da coleta:')
    data_hora_fim = models.DateTimeField(blank=False, help_text='Data e hora final da coleta:')

    # objects = models.GeoManager()

    def __unicode__(self):
        return self.trajetoria
