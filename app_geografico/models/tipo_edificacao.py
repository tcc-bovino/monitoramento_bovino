#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

class TipoEdificacao(models.Model):
    descricao = models.CharField(max_length=255,  blank=True, help_text='Descrição:')

    def __unicode__(self):
        return self.descricao
